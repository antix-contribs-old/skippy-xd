sudo apt install git build-essential fakeroot

cd /tmp
git clone https://gitlab.com/-/ide/project/antix-contribs/skippy-xd

cd /tmp/skippy-xd

sudo apt update

sudo apt install libimlib2-dev libfontconfig1-dev libfreetype6-dev libx11-dev

sudo apt install libxext-dev libxft-dev libxrender-dev zlib1g-dev libxinerama-dev

sudo apt install libxcomposite-dev libxdamage-dev libxfixes-dev

make

sudo make install

cd /tmp && rm -Rf /tmp/spippy-xd

  ###   type  "skippy-xd" to launch the app
  ###
  ###   BEAR IN MIND: IT DOES ITS JOB, THEN IMMEDIATELY EXITS
  ###     TYPICALLY USAGE INVOLVES ASSIGNING IT TO A KEYBIND

 # optional
sudo apt purge libimlib2-dev libfontconfig1-dev libfreetype6-dev libx11-dev

sudo apt purge libxext-dev libxft-dev libxrender-dev zlib1g-dev libxinerama-dev

sudo apt purge libxcomposite-dev libxdamage-dev libxfixes-dev

sudo apt purge git build-essential fakeroot
